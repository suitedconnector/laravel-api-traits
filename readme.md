# SuitedConnector Laravel API Traits

This repository serves as a Composer package containing traits for use by standard SuitedConnector APIs.

## Including in an existing project

These traits are included by default in any project using the [sc-laravel](https://bitbucket.org/suitedconnector/sc-laravel) framework, so if you're using that, no additional steps are necessary.

To include in an existing project:

Add the following in your `composer.json` file

```
    "repositories": [
        {
            "type":"vcs",
            "url": "https://bitbucket.org/suitedconnector/laravel-api-traits.git"
        }
    ],
    "require": {
        "suitedconnector/laravel-api-traits": "dev-master"
    },
```

The traits are added to the `App\Library\Traits` namespace, and will be usable with:

```
use App\Library\Traits\ErrorTrait;
```
