<?php

namespace App\Library\Traits;

use Illuminate\Support\Facades\Log;
use App\Library\Utility\NewRelic;

/* handles the supporting functions for a library serving one of our internal APIs */

trait ErrorTrait {
	protected $_message = '';
	protected $_context = [];
	protected $_success = TRUE;
	protected $_error = FALSE;
	protected $_responseCode = 200;
	protected $_newrelicEventName = '';
	protected $_useNewRelic = FALSE;

	/**
	 *
	 */
	protected function reset() {
		$this->_message = '';
		$this->_context = [];
		$this->_success = TRUE;
		$this->_error = FALSE;
		$this->_newrelicEventName = '';
		$this->_useNewRelic = FALSE;
	}

	/**
	 * @return string
	 */
	public function getNewrelicEventName() {
		return $this->_newrelicEventName;
	}

	/**
	 * @param string $newrelicEventName
	 */
	protected function setNewrelicEventName(string $newrelicEventName) {
		$this->_newrelicEventName = $newrelicEventName;
	}

	/**
	 * @return bool
	 */
	public function useNewRelic(): bool {
		return $this->_useNewRelic;
	}

	/**
	 * @param bool $fireNewRelic
	 */
	protected function setUseNewRelic(bool $fireNewRelic = TRUE) {
		$this->_useNewRelic = $fireNewRelic;
	}

	/**
	 * @return array
	 */
	public function getContext(): array {
		return $this->_context;
	}

	/**
	 * @param array $context
	 */
	protected function setContext(array $context = []) {
		$this->_context = $context;
	}

	/**
	 * @return string
	 */
	public function getMessage(): string {
		return $this->_message;
	}

	/**
	 * @param string $message
	 */
	protected function setMessage(string $message = '') {
		$this->_message = $message;
	}

	/**
	 *
	 */
	protected function setSuccess(string $message = '') {
		$this->setMessage($message);
		$this->setContext();
		$this->_success = TRUE;
		$this->_error = FALSE;
	}

	/**
	 * @param string $message
	 * @param array  $context
	 */
	protected function setFailure(string $message = '', array $context = []) {
		$this->setMessage($message);
		$this->setContext($context);
		$this->_success = FALSE;
	}

	/**
	 * @return bool
	 */
	public function isSuccess(): bool {
		return $this->_success;
	}

	/**
	 * @return bool
	 */
	public function isFailure(): bool {
		return !$this->_success;
	}

	/**
	 * @param string $message
	 * @param array  $context
	 * @param int    $errorCode
	 */
	protected function setError(string $message = '', array $context = [], int $errorCode = 400) {
		$this->setFailure($message, $context);
		$this->_error = TRUE;
		$this->_responseCode = $errorCode;
	}

	/**
	 * @return bool
	 */
	public function isError(): bool {
		return $this->_error;
	}

	/**
	 * @param bool $only_failure
	 */
	protected function log(bool $only_failure = TRUE) {
		if ($this->_success) {
			$prefix = 'SUCCESS: ';
		} else if ($this->_error) {
			$prefix = 'ERROR: ';
		} else {
			$prefix = 'FAILURE: ';
		}

		$log_message = $prefix . get_class() . " {$this->_message}";

		if ($this->_error) {
			// Error
			Log::error($log_message, $this->_context);
		} else if (!$this->_success) {
			// Failure
			Log::warning($log_message, $this->_context);
		} else {
			// Success - if $only_failure is true, do nothing, otherwise log
			if (!$only_failure) Log::info($log_message, $this->_context);
		}
	}

	/**
	 * @return array
	 */
	public function detail(): array {
		return [
			'success' => $this->_success,
			'error' => $this->_error,
			'message' => $this->_message,
			'code' => $this->_responseCode,
			'context' => $this->_context
		];
	}

	/**
	 * @param array $APIResponseDetails
	 */
	protected function consumeAPIDetails(array $APIResponseDetails) {
		$msg = array_key_exists('message', $APIResponseDetails) ? $APIResponseDetails['message'] : '';
		if (array_key_exists('success', $APIResponseDetails) && $APIResponseDetails['success'] === TRUE) {
			$this->setSuccess();
		} else if (array_key_exists('error', $APIResponseDetails) && $APIResponseDetails['error'] === TRUE) {
			$this->setError($msg);
		} else {
			$this->setFailure($msg);
		}
	}

	/**
	 * @param object $errorTraitObject
	 */
	protected function processResponse($errorTraitObject) {
		if ($errorTraitObject->isSuccess()) {
			$this->setSuccess();
		} else {
			if ($errorTraitObject->isError()) {
				$this->setError($errorTraitObject->getMessage());
			} else {
				$this->setFailure($errorTraitObject->getMessage());
			}
		}
	}

	/**
	 *
	 */
	protected function _fireNewRelic() {
		$newRelicContext = $this->_context;
		$newRelicContext['success'] = $this->_success;
		$newRelicContext['result'] = $this->_success ? 'Success' : ($this->_error ? 'Error' : 'Failure');
		$newRelicContext['message'] = $this->_message;
		NewRelic::fireEvent($this->_newrelicEventName, $newRelicContext);
	}

	/**
	 * @return bool
	 */
	protected function results() {
		if (strlen($this->_newrelicEventName)>0 && $this->_useNewRelic) {
			$this->_fireNewRelic();
		}
		return $this->_success;
	}
}
