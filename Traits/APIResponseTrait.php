<?php

namespace App\Library\Traits;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use App\Library\Utility\NewRelic;

trait APIResponseTrait {
	protected $_httpSuccessCode = 200;
	protected $_httpErrorCode = 400;
	protected $_isError = FALSE;
	protected $_isSuccess = FALSE;
	protected $_responseMessage = '';
	protected $_successString = 'success';
	protected $_newrelicEventName = '';
	protected $_useNewRelic = FALSE;
	protected $_context = [];
	protected $_apiPayload = NULL;

	/**
	 *
	 */
	protected function reset() {
		$this->_httpErrorCode = 400;
		$this->_isError = FALSE;
		$this->_isSuccess = FALSE;
		$this->_responseMessage = '';
		$this->_newrelicEventName = '';
		$this->_useNewRelic = FALSE;
		$this->_context = [];
		$this->_apiPayload = NULL;
	}

	/**
	 * @return string
	 */
	protected function getNewrelicEventName() {
		return $this->_newrelicEventName;
	}

	/**
	 * @param string $newrelicEventName
	 */
	protected function setNewrelicEventName(string $newrelicEventName) {
		$this->_newrelicEventName = $newrelicEventName;
		$this->_useNewRelic = TRUE;
	}

	/**
	 * @return bool
	 */
	protected function useNewRelic(): bool {
		return $this->_useNewRelic;
	}

	/**
	 * @param bool $fireNewRelic
	 */
	protected function setUseNewRelic(bool $fireNewRelic = TRUE) {
		$this->_useNewRelic = $fireNewRelic;
	}

	/**
	 * @return array
	 */
	protected function getContext(): array {
		return $this->_context;
	}

	/**
	 * @param array $context
	 */
	protected function setContext(array $context = []) {
		$this->_context = $context;
	}

	/**
	 * @return mixed
	 */
	protected function getAPIPayload() {
		return $this->_apiPayload;
	}

	/**
	 * @param $payload
	 */
	protected function setAPIPayload($payload) {
		$this->_apiPayload = $payload;
	}

	/**
	 * @return Response
	 */
	protected function getResponse() {
		if ((strlen($this->_newrelicEventName) > 0) && $this->_useNewRelic) {
			$this->_fireNewRelic();
		}
		return response(json_encode($this->_getResponseStructure()), $this->_getHTTPCode())->header('Content-Type', 'application/json');
	}

	/**
	 *
	 */
	protected function _fireNewRelic() {
		$newRelicContext = $this->_context;
		$newRelicContext['success'] = $this->_isSuccess;
		$newRelicContext['result'] = $this->_isSuccess ? 'Success' : ($this->_isError ? 'Error' : 'Failure');
		$newRelicContext['message'] = $this->_responseMessage;
		NewRelic::fireEvent($this->_newrelicEventName, $newRelicContext);
	}

	/**
	 * @param bool   $success
	 * @param bool   $error
	 * @param string $message
	 * @param int    $httpCode
	 *
	 * @return Response
	 */
	protected function respond(bool $success, string $message = '', bool $error = FALSE, int $httpCode = NULL) {
		if ($success) {
			$this->setSuccess();
		} else {
			$this->setFailure();
		}
		$this->setMessage($message);
		if ($error) {
			$this->setError();
		}
		if (isset($httpCode)) {
			$this->setErrorCode($httpCode);
		}
		return $this->getResponse();
	}

	/**
	 * @return int
	 */
	protected function _getHTTPCode() {
		return ($this->_isError ? $this->_httpErrorCode : $this->_httpSuccessCode);
	}

	/**
	 * @return array
	 */
	protected function _getResponseStructure() {
		$responseArray = [
			$this->_successString => $this->_isSuccess
		];
		if (strlen($this->_responseMessage)) {
			$responseArray['message'] = $this->_responseMessage;
		}
		if (isset($this->_apiPayload)) {
			$responseArray['payload'] = $this->_apiPayload;
		}
		return $responseArray;
	}

	/**
	 * @param int $errorCode
	 */
	protected function setErrorCode(int $errorCode) {
		$this->_httpErrorCode = $errorCode;
	}


	/**
	 * @param string|NULL $message
	 */
	protected function setError(string $message = NULL) {
		$this->_isError = TRUE;
		$this->_isSuccess = FALSE;
		if (isset($message)) {
			$this->setMessage($message);
		}
	}

	/**
	 *
	 */
	protected function setSuccess(string $message = NULL) {
		$this->_isSuccess = TRUE;
		$this->_isError = FALSE;
		if (isset($message)) {
			$this->setMessage($message);
		}
	}

	/**
	 * @param string $newSuccessString
	 */
	protected function setSuccessString(string $newSuccessString) {
		$this->_successString = $newSuccessString;
	}


	/**
	 * @param string|NULL $message
	 */
	protected function setFailure(string $message = NULL) {
		$this->_isSuccess = FALSE;
		if (isset($message)) {
			$this->setMessage($message);
		}
	}

	/**
	 * @param string $message
	 */
	protected function setMessage(string $message) {
		$this->_responseMessage = $message;
	}

	/**
	 * @param object $errorTraitObject
	 */
	protected function processResponse($errorTraitObject) {
		if ($errorTraitObject->isSuccess()) {
			$this->setSuccess();
		} else {
			if ($errorTraitObject->isError()) {
				$this->setError($errorTraitObject->getMessage());
			} else {
				$this->setFailure($errorTraitObject->getMessage());
			}
		}
	}

	/**
	 * @param bool $only_failure
	 */
	protected function log(bool $only_failure = TRUE) {
		if ($only_failure && (!$this->_isSuccess || $this->_isError)) {
			if ($this->_isSuccess) {
				$prefix = 'SUCCESS: ';
			} else if ($this->_isError) {
				$prefix = 'ERROR: ';
			} else {
				$prefix = 'FAILURE: ';
			}
			$log_message = $prefix . get_class() . " {$this->_responseMessage}";
			if ($this->_isError) {
				Log::error($log_message, $this->_context);
			} else if (!$this->_isSuccess) {
				Log::warning($log_message, $this->_context);
			} else {
				Log::info($log_message, $this->_context);
			}
		}
	}
}
