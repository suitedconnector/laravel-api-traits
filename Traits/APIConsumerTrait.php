<?php

namespace App\Library\Traits;

trait APIConsumerTrait {
	protected $_APISuccess = FALSE;
	protected $_APIMessage = '';
	protected $_APIError = FALSE;

	/**
	 * @param int    $statusCode
	 * @param string $response
	 * @param string $successKey
	 *
	 * @return array
	 */
	protected function evaluateAPIResponse(int $statusCode, string $response, string $successKey = 'success') {
		// by definition, recoverable/reasonable failures return a 200, everything else a 400, so a 400+ is always an ERROR
		if ($statusCode >= 400) {
			// failure
			$this->_APISuccess = FALSE;
			$this->_APIError = TRUE;
		}
		if (trim(strlen($response))) {
			$decoded = json_decode($response, TRUE);
			if (array_key_exists($successKey, $decoded)) {
				$this->_APISuccess = $decoded[$successKey];
			}
			if (array_key_exists('message', $decoded)) {
				$this->_APIMessage = $decoded['message'];
			}
		} else {
			// maybe a regressive case?
			if ($statusCode >= 200 && $statusCode <= 300) {
				$this->_APISuccess = TRUE;
			} else {
				$this->_APIMessage = 'No information provided';
			}
		}
		return $this->APIResponseDetails();
	}

	/**
	 * @return array
	 *
	 * to be consumed by ErrorTrait for its return
	 */
	protected function APIResponseDetails() {
		return [
			'success' => $this->_APISuccess,
			'error' => $this->_APIError,
			'message' => $this->_APIMessage
		];
	}

	/**
	 * @return bool
	 */
	protected function isAPISuccess() {
		return $this->_APISuccess;
	}

	/**
	 * @return bool
	 */
	protected function isAPIError() {
		return $this->_APIError;
	}

	/**
	 * @return bool
	 */
	protected function isAPIFailure() {
		return !$this->_APISuccess;
	}

	/**
	 * @return string
	 */
	protected function getAPIMessage() {
		return $this->_APIMessage;
	}
}
